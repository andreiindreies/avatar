<?php
namespace Training\Avatar\Controller\Manager;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Training\Avatar\Helper\AvatarHelper;

/**
 * Remove Class Avatar's module controller.
 *
 * @package Training\Avatar\Controller\Manager
 */
class Remove extends Action
{
    protected $resultJsonFactory;
    protected $avatarHelper;

    /**
     * Display constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param AvatarHelper $avatarHelper
     */
    public function __construct(Context $context,
                                JsonFactory $resultJsonFactory,
                                AvatarHelper $avatarHelper

    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->avatarHelper = $avatarHelper;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        return $result->setData($this->avatarHelper->removeAvatar());
    }
}
