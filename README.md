# Magento 2 Avatar Module by Andrei Indreies

This Module adds the feature to add your own avatar profile picture for your customer account.


## Magento 2 Avatar Module

- Step 1: Add the module.
- Step 2: Enable the module.
- Step 3: Test it.

### Step 1. Add the module.

Download the module from bitbucket add it to app/code.

### Step 2. Enable the module
**Run these scripts in your terminal in your dir:**
~~~
#php bin/magento module:enable Training_Avatar
~~~
Check if Training_Avatar is enable
~~~
#php bin/magento module:status
~~~
~~~
$php bin/magento setup:di:compile
~~~
~~~
$php bin/magento setup:upgrade
~~~

**Delete cache**
~~~
$php bin/magento cache:clean
~~~
~~~
$php bin/magento cache:flush
~~~

### Step 3. Test it

If you have followed all above steps, you will see Avatar actions when open the url `http://example.com/customer/account/index/`


