<?php
namespace Training\Avatar\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\state\InputMismatchException;
use Magento\Framework\Exception\{
    InputException, LocalizedException, NoSuchEntityException
};

/**
 * Class Customer
 * @package Training\Avatar\Model
 */
class Customer
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    const CUSTOMER_AVATAR_ATTRIBUTE = 'customer_avatar_picture';

    /**
     * Customer constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Save customer avatar in database
     * @param $path
     * @param $id
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws InputMismatchException
     */
    public function setCustomerAvatar($path, $id)
    {
        $customer = $this->customerRepository->getById($id);
        $customer->setCustomAttribute(self::CUSTOMER_AVATAR_ATTRIBUTE, $path);
        $this->customerRepository->save($customer);
    }

    /**
     * Get avatar name from database
     * @param $id
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerAvatar($id)
    {
        $costumer = $this->customerRepository->getById($id);
        $result = $costumer->getCustomAttribute(self::CUSTOMER_AVATAR_ATTRIBUTE)->getValue();

        return $result;
    }
}