<?php
namespace Training\Avatar\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Message\ManagerInterface;
use Training\Avatar\Model\Customer;

/**
 * Class AvatarHelper
 * @package Training\Avatar\Helper
 */
class AvatarHelper extends AbstractHelper
{
    protected $allowedExtensions = ['png','jpeg','jpg'];
    protected $defaultAvatarImgName = 'defaultprofile.png';
    protected $fileId = 'avatar';
    protected $mediaDirectory;
    protected $fileUploaderFactory;
    protected $customerSession;
    protected $file;
    protected $target;
    protected $storeManager;
    protected $messageManager;
    protected $customer;

    /**
     * AvatarHelper constructor.
     * @param UploaderFactory $fileUploaderFactory
     * @param Filesystem $filesystem
     * @param Session $customerSession
     * @param DirectoryList $directoryList
     * @param File $file
     * @param StoreManagerInterface $storeManager
     * @param ManagerInterface $messageManager
     * @param Customer $customer
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function __construct(UploaderFactory $fileUploaderFactory,
                                    Filesystem $filesystem,
                                    Session $customerSession,
                                    DirectoryList $directoryList,
                                    File $file,
                                    StoreManagerInterface $storeManager,
                                    ManagerInterface $messageManager,
                                    Customer $customer

        ) {
            $this->mediaDirectory = $filesystem->getDirectoryWrite($directoryList::MEDIA);
            $this->fileUploaderFactory = $fileUploaderFactory;
            $this->customerSession = $customerSession;
            $this->file = $file;
            $this->storeManager = $storeManager;
            $this->messageManager = $messageManager;
            $this->customer = $customer;
        }

    /**
     * Remove the avatar file only from database
     * Setting avatar's name - to load the default image instead
     *
     * @return array
     */
    public function removeAvatar()
    {
        $result = array('Value' => true, 'Message' => 'Avatar deleted successfully');

        try {
            $this->setAvatar($this->defaultAvatarImgName, $this->getCurrentCustomerId());
        } catch (\Exception $e) {
            $result['Value'] = false;
            $result['Message'] = $e->getMessage();
        }

        $this->messageDisplay($result);

        return $result;
    }

    /**
     * Upload the avatar message and set customer's avatar name in database
     *
     * @param $customerId
     * @return array
     */
    public function addAvatar($customerId = null)
    {
        if (!$customerId) {
            $customerId = $this->getCurrentCustomerId();
        }

        $avatarsPath = "customer/{$customerId}";
        $target = $this->mediaDirectory->getAbsolutePath($avatarsPath);
        $result = array('Value' => true, 'Message' => 'Avatar uploaded successfully');

        try {
            $uploader = $this->fileUploaderFactory->create(['fileId' => $this->fileId]);
            $uploader->setAllowedExtensions($this->allowedExtensions);
            $uploader->setAllowRenameFiles(true);
            $resultUpload = $uploader->save($target);
            $fileName = $customerId . "/" . $uploader->getUploadedFileName();
            $this->setAvatar($fileName, $customerId);

            if (!$resultUpload['file']) {
                $result['Value'] = false;
                $result['Message'] = 'An error has occurred on avatar upload.';
            }
        } catch (\Exception $e) {
            $result['Value'] = false;
            $result['Message'] = $e->getMessage();
            $result['Error_Code'] = $e->getCode();
        }

        $this->messageDisplay($result);

        return $result;
    }

    /**
     * Returns customer Avatar's Url.
     *
     * @return string
     */
    public function getAvatarUrl($id = null)
    {

        try {
            $mediaDir = $this->storeManager->getStore()->getBaseUrl() . "pub/media/customer";

            if (!$id) {
                $id = $this->getCurrentCustomerId();
            }

            $avatar = $this->getAvatar($id);

            if ($avatar[0] != '/') {
                $avatar = "/" . $avatar;
            }

            $url =  $mediaDir . $avatar;

        } catch(\Exception $e) {
            return $e->getMessage();
        }

        return $url;
    }

    /**
     * @param $fileName
     * @param $id
     */
    public function setAvatar($fileName, $id)
    {
        $this->customer->setCustomerAvatar($fileName, $id);
    }

    /**
     * Get the avatar's name
     * @param $id
     * @return mixed
     */
    public function getAvatar($id)
    {
        return $this->customer->getCustomerAvatar($id);
    }

    /**
     * Returns current customer's id from session.
     *
     * @return int|null
     */
    public function getCurrentCustomerId()
    {
        return $this->customerSession->getCustomerId();
    }

    /**
     * Display the result message by result value.
     *
     * @param $result
     * @return ManagerInterface
     */
    private function messageDisplay($result)
    {
        $message = $result['Message'];

        if (!$result['Value']) {
            return $this->messageManager->addErrorMessage(__($message));
        }

        return $this->messageManager->addSuccessMessage(__($message));
    }

}