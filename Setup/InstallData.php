<?php
namespace Training\Avatar\Setup;

use Magento\Customer\Model\Metadata\CustomerMetadata;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;

/**
 * This adds the new custom attribute for customers entity.
 * Class InstallData
 * @package Training\Avatar\Setup
 */
class InstallData implements InstallDataInterface
{
    const CUSTOM_ATTRIBUTE_CODE = 'customer_avatar_picture';

    /**
     * @var EavSetup
     */
    private $eavSetup;
    /**
     * @var Config
     */
    private $eavConfig;

    public function __construct(EavSetup $eavSetup, Config $config)
    {
        $this->eavSetup = $eavSetup;
        $this->eavConfig = $config;
    }

    public function Install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->eavSetup->addAttribute(
          CustomerMetadata::ENTITY_TYPE_CUSTOMER, self::CUSTOM_ATTRIBUTE_CODE,
          [
                'label' => 'AvatarImg',
                'input' => 'file',
                'visible' => true,
                'required' => false,
                'position' => 150,
                'sort_order' => 150,
                'system' => false,
          ]
        );

        $avatarImg = $this->eavConfig->getAttribute(CustomerMetadata::ENTITY_TYPE_CUSTOMER, self::CUSTOM_ATTRIBUTE_CODE);
        $avatarImg->setData('used_in_forms', ['adminhtml_customer', 'customer_account_create', 'customer_account_edit']);

        $avatarImg->save();

        $setup->endSetup();
    }

}
