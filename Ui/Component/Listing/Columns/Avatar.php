<?php
namespace Training\Avatar\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface ;
use Magento\Framework\View\Asset\Repository;

use Training\Avatar\Helper\AvatarHelper;

/**
 * Class Avatar
 * @package Training\Avatar\Ui\Component\Listing\Columns
 */
class Avatar extends Column
{
    /**
     * @var \Magento\Framework\View\Element\AbstractBlock
     */
    protected $viewFileUrl;
    protected $urlBuilder;
    protected $avatarHelper;

    /**
     * Avatar constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param Repository $viewFileUrl
     * @param AvatarHelper $avatarHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(ContextInterface $context,
                                UiComponentFactory $uiComponentFactory,
                                UrlInterface $urlBuilder,
                                Repository $viewFileUrl,
                                AvatarHelper $avatarHelper,
                                array $components = [],
                                array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
        $this->viewFileUrl = $viewFileUrl;
        $this->avatarHelper = $avatarHelper;
    }

    /**
     * Append avatar url to every customer by his entity Id
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');

            foreach ($dataSource['data']['items'] as & $item) {
                $picture_url = $this->avatarHelper->getAvatarUrl($item['entity_id']);

                $item[$fieldName . '_src'] = $picture_url;
                $item[$fieldName . '_orig_src'] = $picture_url;
                $item[$fieldName . '_alt'] = 'The profile picture';
            }
        }

        return $dataSource;
    }
}