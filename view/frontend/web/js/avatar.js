define([
    "jquery"
], function (s) {
    "use strict";
    return function(config, element) {
        document.getElementById("delete-button").addEventListener("click", function() {
            deleteAvatar(config);
        });

        document.getElementById('upload-button').addEventListener('click', function() {
            document.getElementById('upload-file').click();

        });

        document.getElementById('upload-file').addEventListener('change', function() {
            uploadAvatar(config);
        });
    }
});

function deleteAvatar(config) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            updateAvatar(config);
        }
    };

    xhttp.open("DELETE", config.url + 'remove', true);
    xhttp.send();
}

function uploadAvatar(config) {
    var data = new FormData();
    var xhttp = new XMLHttpRequest();

    data.append('avatar', document.getElementById('upload-file').files[0]);
    xhttp.onload = function() {
        updateAvatar(config);
    };

    xhttp.open('POST', config.url + 'upload');
    xhttp.send(data);
}

function updateAvatar(config) {
    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var avatarImg = document.getElementById('avatarImg');
            avatarImg.src = JSON.parse(this.responseText)["url"];
        }
    };

    xhttp.open("GET", config.url + 'display', true);
    xhttp.send();
}

