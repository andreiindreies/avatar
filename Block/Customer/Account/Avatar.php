<?php
namespace Training\Avatar\Block\Customer\Account;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session;
use Training\Avatar\Helper\AvatarHelper;

/**
 * Class Avatar block for Avatar module.
 *
 * @package Training\Avatar\Block\Customer\Account
 */
class Avatar extends Template {
    protected $customerRepository;
    protected $customerSession;
    protected $mediaDirectory;
    protected $avatarHelper;

    /**
     * Avatar constructor.
     *
     * @param Context $context
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(Context $context,
                                CustomerRepositoryInterface $customerRepository,
                                Session $customerSession,
                                AvatarHelper $avatarHelper
    )
    {
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->avatarHelper = $avatarHelper;

        parent::__construct($context);
    }

    /**
     * Get customer's avatar url
     *
     * @return string
     */
    public function getAvatarUrl()
    {
        return $this->avatarHelper->getAvatarUrl();
    }
}